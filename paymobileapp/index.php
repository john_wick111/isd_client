<?php session_start();
$_SESSION["order_id"] = $_GET['id']; 
$_SESSION["amount"] = $_GET['amount'];
$_SESSION["client_id"] = $_GET['client_id'];
$_SESSION["rent"] = $_GET['rent'];
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title> Pay Order </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/paymobileapp/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/paymobileapp/assets/css/normalize.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/paymobileapp/assets/css/fontello.css">
        <link rel="stylesheet" href="/paymobileapp/assets/css/style.css">
        <!--<link rel="stylesheet" href="assets/css/style2.css">-->
        <link rel="stylesheet" href="https://designmodo.com/demo/creditcardform/style.css">

    </head>
    <body>
       <header>
            <div class="logo">
                
            </div>
        </header>
        <div class="wrapper">

    <section class="payment-method">
        <ul>
             <li>
                <div class="details" >
                    
                    <h3>Payment Card Details</h3>
                    <form id="frm_payfort_payment_merchant_page2" class="form-horizontal">
                        <div class="form-group">
                            <div class="col-sm-9">
                                <label>Full name on the card</label>
                                <input type="text" class="form-control" value="" name="card_holder_name" id="payfort_fort_mp2_card_holder_name"  maxlength="50">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-9">
                                <label>Credit card number</label>
                                <input type="text" class="form-control" value="" name="card)number" id="payfort_fort_mp2_card_number" maxlength="16">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>Month</label>
                                        <select class="form-control col-sm-2" name="expiry_month" id="payfort_fort_mp2_expiry_month">
                                            <option value="01">January</option>
                                            <option value="02">February </option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>

                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Year</label>
                                        <select class="form-control" name="expiry_year" id="payfort_fort_mp2_expiry_year">
                                            <option value="21"> 2021</option>
                                            <option value="22"> 2022</option>
                                            <option value="23"> 2023</option>
                                            <option value="24"> 2024</option>
                                            <option value="25"> 2025</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label>CCV</label>
                                <input type="text" class="form-control" name="cvv"  id="payfort_fort_mp2_cvv" maxlength="4">
                            </div>
                        </div>
                    </form>
                        <section class="actions">
                            <a class="continue" id="btn_continue" href="javascript:void(0)">Pay</a>
                        </section>
                </div>
            </li>
        </ul>
    </section>


    <script type="text/javascript" src="/paymobileapp/vendors/jquery.min.js"></script>
        <script type="text/javascript" src="/paymobileapp/assets/js/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="/paymobileapp/assets/js/checkout.js"></script>
        <script type="text/javascript">
		  
		
            $(document).ready(function () {
				
				
                //getPaymentPage("cc_merchantpage");
				
                $('input:radio[name=payment_option]').click(function () {
                    $('input:radio[name=payment_option]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).addClass('active');
                            $(this).parent('li').children('label').css('font-weight', 'bold');
                            $(this).parent('li').children('div.details').show();
                        }
                        else {
                            $(this).removeClass('active');
                            $(this).parent('li').children('label').css('font-weight', 'normal');
                            $(this).parent('li').children('div.details').hide();
                        }
                    });
                });
                $('#btn_continue').click(function () {
                    var paymentMethod = "cc_merchantpage2";
                    if(paymentMethod == 'cc_merchantpage2') {
                        var isValid = payfortFortMerchantPage2.validateCcForm();
                        if(isValid) {
                            getPaymentPage(paymentMethod);
                        }
                    }
                    else{
                        getPaymentPage(paymentMethod);
                    }
                });
            });
        </script>
<?php include('footer.php') ?>