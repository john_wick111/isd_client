<ul class="unstyled sidebar-navigation">
    
    <li>
        <a href="/client/bookings" class="active">
        <i class="xicon">
            <img src="https://isddubai.com/assets-web/images/icons/dashboard.svg" alt="">
        </i> Dashboard</a>
    </li>
    <li>
        <a href="/client/all-bookings" class="disable">
        <i class="xicon">
            <img src="https://isddubai.com/assets-web/images/icons/booking.svg" alt="">
        </i> Review Booking</a>
    </li>
    
    <li>
        <a href="/client/all-academy-inquires" class="disable">
        <i class="xicon">
            <img src="https://isddubai.com/assets-web/images/icons/booking.svg" alt="">
        </i> Academy Inquires</a>
    </li>
    
      <li>
        <a href="{{route('academy')}}"><i class="xxicon icon-credit-card"></i>Academy</a>
    </li>
    
     <li>
        <a href="{{route('client.payment-cards')}}"><i class="xxicon icon-credit-card"></i>Account</a>
    </li>
    
    

    <li>
        <a href="https://meal.thewebagency.me/account/change-password" class="disable"><i class="xxicon icon-credit-card"></i>Change Password</a>
    </li>
    
    <li>
        <a href="{{route('logout')}}"><i class="xxicon icon-credit-card"></i>Logout</a>
    </li>
</ul>