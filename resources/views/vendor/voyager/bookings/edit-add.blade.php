@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            id="formdata"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
      
                                </div>
                                @if($row->getTranslatedAttribute('display_name') == 'Client')
                                {{-- <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Name</label>
                                    <input type="text" class="form-control" id="name" disabled value="">
                                </div>     --}}
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Email</label>
                                    <input type="text" class="form-control" id="email" disabled value="">
                                </div>    
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Contact</label>
                                    <input type="text" class="form-control" id="contacts" disabled value="">
                                </div> 
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Contact Emg</label>
                                    <input type="text" class="form-control" id="contacts_emg" disabled value="">
                                </div>                                                                                                                                                  
                                @endif                                
                            @endforeach
                            <input type="hidden" name="model" value="booking">

                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name"></label>
                                @if($edit)
                                <input type="hidden" name="sports" value="{{$dataTypeContent->sports}}">
                                <input type="hidden" name="id" value="{{$dataTypeContent->getKey()}}">
                                <div class="input_fields_wrap">   
                                    <table class="table">
                                        <tbody class="thead-dark products">
                                            <tr>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Count</th>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <?php $OrderProduct = App\OrderProduct::where('book_id',$dataTypeContent->getKey())->get(); ?>
                                            @foreach($OrderProduct as $items)   
                                            <tr>
                                                <td>
                                                    <select name="title[]" id="0" class="form-control title">
                                                        <?php $products = DB::table('products')->where('stock', '>', 0)->get();   ?>
                                                        @foreach ($products as $item)
                                                            <option value="{{$item->id}}" @if($items->title == $item->id) selected @endif>{{$item->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="number" id="0" class="form-control price_product0" value="{{$items->price}}" readonly  name="price_product[]"></td>
                                                <td><input type="number" id="0" class="form-control count_product0" value="{{$items->count}}" name="count[]"></td>
                                                <td><input type="number" id="0" class="form-control total_product0" value="{{$items->total}}" value="0"  readonly  name="total_product[]"></td>
                                                <td><a href="{{ url('order/product',$items->id)}}" class="delete-row btn btn-danger mb-2">Delete</a></td>
                                            </tr>
                                            @endforeach
                                        </tfoot>
                                    </table>   
                                    <button class="add_field_button btn btn-primary mb-2">Add New Product</button>   
                                </div>
                                @else 
                                
                                <div class="input_fields_wrap">   
                                    <table class="table">
                                        <tbody class="thead-dark products">
                                            <tr>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Count</th>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </tbody>
                                        <tfoot> 
                                            <tr>
                                                <td>
                                                    <select name="title[]" id="0" class="form-control title">
                                                        <option value="">Select product</option>
                                                        <?php $products = DB::table('products')->where('stock', '>', 0)->get();   ?>
                                                        @foreach ($products as $item)
                                                            <option value="{{$item->id}}">{{$item->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="number" id="0" class="form-control price_product0" name="price_product[]"></td>
                                                <td><input type="number" id="0" class="form-control count_product0" name="count[]"></td>
                                                <td><input type="number" id="0" class="form-control total_product0" value="0"  readonly  name="total_product[]"></td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>   
                                    <button class="add_field_button btn btn-primary mb-2">Add New Product</button>   
 
                                </div>                           
                                @endif                                                                  
                            </div>  

                            <div class="form-group  col-md-12 ">
                                <label class="control-label" for="name"></label>
                                {{-- <button class="add_field_button btn btn-primary mb-2">Add New Book</button>    --}}
                                @if($edit)
                                <div class="input_fields_wrap">   
                                    <table class="table">
                                        <tbody class="thead-dark">
                                            <tr>
                                                <th>Date</th>
                                                <th>Time From</th>
                                                <th>Time To</th>
                                                <th>Total hours</th>
                                                <th>Court</th>
                                                <th>Court size</th>
                                                <th>Price</th>
                                                <th>Discount</th>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <?php $court_book = DB::table('bookings')->where('id',$dataTypeContent->getKey())->get(); ?>
                                            @foreach($court_book as $book)                                            
                                            <tr>
                                                <td><input type="date" value="{{$book->date}}" id="0" class="form-control date0" name="date"></td>
                                                <td><input type="time" value="{{$book->from}}" id="0" class="form-control from0" name="from"></td>
                                                <td><input type="time" value="{{$book->to}}" id="0" class="form-control to0" name="to"></td>
                                                <td><input type="text" id="0" class="form-control hours0" disabled value="0" name="hours" /></td>
                                                <td>
                                                    <select name="pitch" id="0" class="form-control pitch">
                                                        <option value="">Select Court</option>
                                                        <?php $tiems = DB::table('courts')->get();   ?>
                                                        @foreach ($tiems as $item)
                                                        <option value="{{$item->id}}" @if($book->pitch == $item->id) selected @endif>{{$item->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" value="{{$book->size}}" class="form-control size0" readonly name="size"></td>
                                                <td><input type="number" value="{{$book->price}}" class="form-control price0" readonly name="price"></td>
                                                <td><input type="number" value="{{$book->dicsount}}"class="form-control discount0" id="0" name="discount"></td>
                                                <td><input type="number" value="{{$book->total}}" class="form-control total0" name="total"></td>
                                            </tr>
                                            @endforeach
                                        </tfoot>
                                    </table>     
                                </div>
                                @else 
                                
                                <div class="input_fields_wrap">   
                                    <table class="table">
                                        <tbody class="thead-dark">
                                            <tr>
                                                <th>Date</th>
                                                <th>Time From</th>
                                                <th>Time To</th>
                                                <th>Total hours</th>
                                                <th>Sport</th>
                                                <th>Court</th>
                                                <th>Court size</th>
                                                <th>Price</th>
                                                <th>Discount</th>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td><input type="date" min="<?php echo date('Y-m-d'); ?>" id="0" class="form-control date0" name="date" value="{{old('date')}}" required></td>
                                                <td><input type="time" id="0" class="form-control from0" name="from" value="{{old('from')}}" required></td>
                                                <td><input type="time" id="0" class="form-control to0" name="to" value="{{old('to')}}" required></td>
                                                <td><input type="text" id="0" class="form-control hours0" disabled value="0" name="hours" value="{{old('hours')}}" required /></td>
                                                <td>
                                                    <select name="sport" id="0" class="form-control sport0" required>
                                                        <option value="">Select Sport</option>
                                                        <?php $tiems = DB::table('sports')->get();   ?>
                                                        @foreach ($tiems as $item)
                                                        @if (old('sport') == $item->id)
                                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                        @else
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                        @endif

                                                        @endforeach                                                        
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="pitch" id="0" class="form-control pitch0" required>
                                                        <option value="">Select Court</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" class="form-control size0" readonly name="size" value="{{old('size')}}"></td>
                                                <td><input type="number" class="form-control price0" readonly name="price" value="{{old('price')}}"></td>
                                                <td><input type="number" class="form-control discount0" id="0" name="discount" value="{{old('discount')}}" required></td>
                                                <td><input type="number" class="form-control total0" name="total" value="{{old('total')}}"></td>
                                            </tr>
                                        </tfoot>
                                    </table>     
                                </div>                           
                                @endif                                                                  
                            </div>                                
                        </div><!-- panel-body -->
                        
                        <div class="errorTxt" id="newsletterValidate"></div>
                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();


            function calculate() {

            }

            $(document).ready(function() { 
                var day;
                @if($edit)
                var splashArray = $('[name="sports"]').val()
                @else
                var splashArray = [];
                @endif
                var productsArray = [];
                console.log(calculate());
                var add_button      = $(".add_field_button"); //Add button ID
                var x = 0; //initlal text box count
                $(add_button).click(function(e){ //on add input button click
                    e.preventDefault();
                    x++; //text box increment
                     $(".products tr:last").after('<tr><td><select name="title[]" id="'+x+'" class="form-control title'+x+'"><option value="">Select Court</option><?php $products = DB::table('products')->get();   ?>@foreach ($products as $item)<option value="{{$item->id}}">{{$item->title}}</option>@endforeach</select></td><td><input type="number" id="'+x+'" class="form-control price_product'+x+'" readonly name="price_product[]"></td><td><input type="number"  id="'+x+'" class="form-control count_product'+x+'" name="count[]"><td><input type="number" id="'+x+'" class="form-control total_product'+x+'" value="0" readonly   name="total_product[]"></td></td><td><button type="button" class="delete-row btn btn-danger mb-2">Delete</button></td></tr>');
                });
                $("table").on("click", ".delete-row", function (event) {
                    $(this).closest("tr").remove();   
                });

                $(".delete_time").click(function(e){ //on add input button click
                    e.preventDefault();
                    var id = $(this).attr('id');
                    $('#item'+id).remove();
                    $.ajax({
                        url: "/delete/"+id,
                        type: 'GET',
                        dataType: 'json', // added data type
                        success: function(res) {
                            console.log(res);
                        }
                    });
               });

            $('.save').on('click', function(e) {
                e.preventDefault();
                var client_id = $(".select2-ajax").val();
                var status = $("select[name=status]").val();
                var penalty = $("input[name=penalty]").val();
                var from = $('.from0').val()
                var to = $('.to0').val()
                var date = $('.date0').val()
                var sport = $(".sport0").val();
                var pitch = $('.pitch0').val()
                var size = $("input[name=size]").val();
                var price = $("input[name=price]").val();
                var discount = $("input[name=discount]").val();
                var total = $("input[name=total]").val();
                if(client_id == ""){
                    toastr.error("client is required");
                    return;
                }
                if(penalty == ""){
                    toastr.error("penalty is required");
                    return;
                }
                if(date == ""){
                    toastr.error("date is required");
                    return;
                }
                if(from == ""){
                    toastr.error("time from is required");
                    return;
                }
                if(to == ""){
                    toastr.error("time to is required");
                    return;
                } 
                if(sport == ""){
                    toastr.error("sport to is required");
                    return;
                }
                if(pitch == ""){
                    toastr.error("Court to is required");
                    return;
                }                                                                               
                $.ajax({
                    url: '/check/book',
                    type: "POST",
                    data:  $("#formdata").serialize()+ '&' +'sports='+splashArray,
                    dataType: 'json',
                    success: function(response){ 
                        console.log(response); 
                        console.log(response.state);  
                        if(response.state == 200){
                            window.location.href = "{{url('admin/bookings')}}";
                            toastr.success(response.msg);
                        }else{
                            toastr.error(response.msg);
                        }                             
                    }
                });
            });

            $('select[name="client_id"]').on('change', function() {
               console.log("this.value"+this.value);
                $.ajax({
                    url: '/api/v1/users/'+this.value,
                    type: "Get",
                    dataType: 'json',
                    success: function(response){ 
                        console.log(response);
                        $("#name").val(response.data.name); 
                        $("#email").val(response.data.email);
                        $("#contacts").val(response.data.contacts);                               
                        $("#contacts_emg").val(response.data.contacts_emg);                               
                    }
                });
            });

            $(document).on("change",'select[name="title[]"]', function(){
                var id = $(this).attr('id');
                console.log("value"+this.value);
                $.ajax({
                    url: '/check/products/'+this.value,
                    type: "Get",
                    dataType: 'json',
                    success: function(response){ 
                        console.log(response);   
                        $('.price_product'+id).val(response.data.price);                            
                    }
                });
            });

            $(document).on("change paste keyup",'input[name="count[]"]', function(){
                var id = $(this).attr('id');
                console.log("value"+this.value);
                var price = $('.price_product'+id).val();
                var total =  this.value * price
                $('.total_product'+id).val(total); 
            });

            $(document).on("change",'select[name="pitch"]', function(){
                console.log("value"+this.value);
                splashArray = [];
                var id = $(this).attr('id');
                console.log(id);
                    $.ajax({
                        url: '/api/v1/courts/'+this.value,
                        type: "Get",
                        dataType: 'json',
                        success: function(response){ 
                            console.log(response.data.sport);
                            var hour = $('.hours'+id).val();
                            var all = response.data.rate * hour
                            $('.price'+id).val(all); 
                            $(".size"+id).val(response.data.size);
                            response.data.sport.forEach(function(item) {
                                splashArray.push(item['sport_id']);
                            });
                        }
                    });
                });
            });

            $(document).on("change paste keyup",'select[name="sport"]', function(){
                var id = $(this).attr('id');
                var sport = $('.sport'+id).val()
                var from = $('.from'+id).val()
                var to = $('.to'+id).val()
                var pitch = $('.pitch'+id).val()
                var date = $('.date'+id).val()

                console.log(day);
                $.ajax({
                    url: '/check/sport',
                    type: "POST",
                    data:{
                        sport:sport,
                        day:day,
                        from:from,
                        to:to,
                    },
                    dataType: 'json',
                    success: function(response){ 
                        $('.pitch'+id).html('');
                        console.log(response);
                        jQuery('<option/>', {
                                value: '',
                                html: 'Select Court'
                                }).appendTo('.pitch'+id);
                        for(var i=0; i< response.data.length;i++)
                        {
                            console.log(response.data[i]);
                                jQuery('<option/>', {
                                value: response.data[i]['id'],
                                html: response.data[i]['name']
                                }).appendTo('.pitch'+id);
                        }                        
                    }
                });                
            });

            $(document).on("change paste keyup",'input[name="date"]', function(){
                var id = $(this).attr('id');
                day = new Date($(".date"+id).val()).getDay();
                switch (new Date($(".date"+id).val()).getDay()) {
                case 0:
                    day = "Sunday";
                    break;
                case 1:
                    day = "Monday";
                    break;
                case 2:
                    day = "Tuesday";
                    break;
                case 3:
                    day = "Wednesday";
                    break;
                case 4:
                    day = "Thursday";
                    break;
                case 5:
                    day = "Friday";
                    break;
                case 6:
                    day = "Saturday";
                }                
                console.log(day);
                // $.ajax({
                //     url: '/check/courts/'+day,
                //     type: "Get",
                //     dataType: 'json',
                //     success: function(response){ 
                //         console.log(response.data);
                //         for(var i=0; i< response.data.length;i++)
                //         {
                //             console.log(response.data[i]);
                //         jQuery('<option/>', {
                //                 value: response.data[i]['court_id'],
                //                 html: response.data[i]['court']['name']
                //                 }).appendTo('.pitch0');
                //         }                        
                //     }
                // });                
            });


            $(document).on("change paste keyup",'input[name="discount"]', function(){
                var id = $(this).attr('id');
                var value = $(".price"+id).val() * this.value / 100;
                console.log(value);
                var v = $(".price"+id).val() - value
                $('.total'+id).val(v);
            });


            $(document).on("change",'input[name="from"],input[name="to"]', function(){
                var id = $(this).attr('id') ?  $(this).attr('id') : 0;
                var hours = parseInt($(".to"+id).val().split(':'), 10) - parseInt($(".from"+id).val().split(':'), 10);
                var minuts = parseInt($(".to"+id).val().split(':')[1], 10) - parseInt($(".from"+id).val().split(':')[1], 10);
                if(minuts > 0){
                    hours += 1
                }
                $(".hours"+id).val(hours);                
            });

            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
