@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', 'login')
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('https://isddubai.com/assets-web/images/banners/football-venue.webp');">
</section>


<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">
        
        <h2 class="maintitle">
            <span class="fc-football">My ISD Login</span>
        </h2>

        <hr class="divider">

        <p class="maindesc --big mt-20 mb-40">
            Welcome to My ISD, where you can access all of ISD at your fingertips! Book your favourite pitch or court, register your children in one of ISD’s world-class academies, and retrieve your booking anytime, anywhere! <br><br>
            If you are new to ISD, create your account below. You can use the same login for this website and for MY ISD app on iOS and Android!
        </p>

        <div class="content-section mb-40">
            <div class="row">
                <section class="col-xl-5 col-lg-6">
                    
                    <div class="box --registration-box">
                        {!! Form::open(['route' => 'login', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form --registration-form"]) !!}
                        <div class="control-group">
                            <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="{{ __('E-Mail Address') }}" autofocus>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="control-group">
                            <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required  placeholder="{{ __('Password') }}">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="control-group">
                            <button type="submit" class="btn --btn-primary">
                            {{ __('Login') }}
                            </button>
                        </div>
                        
                        {!! Form::close() !!}
                    </div>

                    <a href="/register" class="btn --football fc-white">
                        {{ __('Create account') }}
                    </a>
                </section>
            </div>
        </div>
    </div>
</section>

@endsection