@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->

                             <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    {{-- <link rel="stylesheet" href="https://frontendscript.com/demo/jquery-timepicker-24-hour-format/dist/wickedpicker.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet"/> --}}

    {{-- <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script> --}}

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">Book your session at ISD!</h2>

            <p class="maindesc --big">
                Please choose your sport, pitch or court, booking day, time & duration, from the drop-down menu.
                <br>
                If you have any questions, please call us on 04 448 1555 and we will more than happy to help you.
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    {!! Form::open(['route' => 'submitAcademy', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data", 'class'=>'default-form']) !!}
    
                        <!-- Default Card Example -->
                        
                            
                        @if(!empty($error_status) && $error_status==1)
                        <div class="alert --danger" role="alert">
                            <strong>Oops!</strong> No Courts is Found
                        </div>
                        @endif
                        

                        <div class="row">
                            <div class="col-md-12">
                                {{-- dd(request()->my_checkbox) --}}
                                 <input type="hidden" name="package_name" id="package_name" value="{{ request()->package_name }}"/>
                                  <input type="hidden" name="cities" id="cities" value="{{ request()->cities }}"/>
            <input type="hidden" name="academy" id="academy" value="{{ request()->academy }}"/>
            <input type="hidden" name="childname" id="childname" value="{{ request()->childname }}"/>  
            <input type="hidden" name="dob" id="dob" value="{{ request()->dob }}"/>
            <input type="hidden" name="age_category" id="age_category" value="{{ request()->age_category }}"/>
            <input type="hidden" name="my_checkbox" id="my_checkbox" value="{{ request()->my_checkbox }}"/>
              <input type="hidden" name="terms" id="terms" value="{{ request()->terms }}"/>
                <input type="hidden" name="sport" id="sport" value="{{ request()->sport }}"/>
                <input type="hidden" name="age" id="age" value="2012-03-01"/>
                 <input type="hidden" name="day_num_count" id="day_num_count" value=""/>

                    <div class="control-group">
                        {!! Form::label('date', 'Start Date', ['class' => 'form-label']) !!}
                          {{ Form::text('start_date', $request->date ?? null, ['class' => 'form-field','id'=>'date','autocomplete'=>"off",'required'=>'required']) }}
                    </div>
                    
                    {{-- @if(request()->academy == 2)
                         <div class="control-group">
                     
                             {!! Form::label('total_days', 'Number of Days', ['class' => 'form-label']) !!}
                            <select id="total_days" class="form-field" name="total_days" required autofocus>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                        
                         </div>
                    @endif --}}
                    
                    {{-- @if(request()->academy == 3)
                         <div class="control-group">
                     
                            {!! Form::label('total_days', 'Number of Days', ['class' => 'form-label']) !!}
                            <select id="total_days" class="form-field" name="total_days" required autofocus>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        
                         </div>
                    @endif --}}
                    
                    
                    <div class="control-group">
                         {!! Form::label('time', 'Training Time', ['class' => 'form-label','id'=>'training_time']) !!}
                                                                 
                         @foreach($classes as $class) 
                         
                         {{-- old code  ---}}
                         {{-- {!! Form::radio('cls', $class['id'], false,['id' => $class['id']]) !!}  <label for="{{ $class['id'] }}">{{ $class['title'] }}</label> <br> --}}
                         {!! Form::checkbox('num_days[]', $class['id'], false,['id' => $class['id']]) !!}  <label for="{{ $class['id'] }}">{{ $class['title'] }}</label> <br>
                         
                         @endforeach
                         
                    </div>
                    
                     {{-- <div class="control-group">
                         
                         <input type="checkbox" id="Saturday" class="checkBox"   name="num_days[]" value="Saturday">
                                <label for="Saturday">Saturday</label><br>
                                <input type="checkbox" id="Sunday" class="checkBox"    name="num_days[]"value="Sunday">
                                <label for="Sunday">Sunday</label><br>
                                <input type="checkbox" id="Monday" class="checkBox"   name="num_days[]"value="Monday">
                                <label for="Monday">Monday</label><br>
                                <input type="checkbox" id="Tuesday" class="checkBox"   name="num_days[]"value="Tuesday">
                                <label for="Tuesday">Tuesday</label><br>
                                <input type="checkbox" id="Wednesday" class="checkBox"    name="num_days[]"value="Wednesday">
                                <label for="Wednesday">Wednesday</label><br>
                                <input type="checkbox" id="Thursday" class="checkBox"   name="num_days[]"value="Thursday">
                                <label for="Thursday">Thursday</label><br>
                                <input type="checkbox" id="Friday" class="checkBox"   name="num_days[]"value="Friday">
                                <label for="Friday">Friday</label><br>                                               
                         
                         
                     </div> --}}
                     
                        <div class="control-group" id="cost">
                    
                            {!! Form::label('cost-amnt', 'Total Amount', ['class' => 'form-label']) !!}
                            <p id="cost-amnt" class="cost" style="font-weight:bold;font-size:30px;"></p>
                            </div>
                            
  
                        </div>

                            <input type="hidden" id="total-amnt" name="cost">
                        
                        <div class="control-group">
                            <button type="submit" class="btn --football fc-white" id="btn-sbmt" name="submit" value="1">Submit</button>
                            <button type="button" class="btn --football fc-white" id="btn-reset" name="reset" value="1">Reset</button>
                        </div>

                    {!! Form::close() !!}
                    
                      <button type="button" id="btnCalculate" class="btn --football fc-white" value="1">Calculate</button>
                          
                        
                    <form method="post" action="{{route('client.reset.form')}}" id="hiddenForm" enctype="multipart/form-data" hidden>

                        <input type="hidden" value="{{$request->sport ?? null}}" name="sport">
                        <input type="hidden" value="{{$request->time ?? null}}" name="time">
                        <input type="hidden" value="{{$request->pitch ?? null}}" name="pitch">
                        <input type="hidden" value="{{$request->date ?? null}}" name="date">
                        <input type="hidden" value="{{$request->duration ?? null}}" name="duration">
                        
                        <input type="submit" value="submit" name="submit" id="hidden-btn">
                    </form>
                        
                    <div class="sport-detail">
                        <div class="football-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Indoor Artificial</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg')}}' alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Outdoor Artificial</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg')}}' alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Outdoor Grass</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg')}}' alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tennis-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 1</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg')}}' alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 2</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg')}}' alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 3</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg')}}' alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="athletics-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Athletics Track</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg')}}' alt='athletics-sport' class='img-fluid'>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>    

<script>

$(document).ready(function(){
    
    var academy = $('#package_name').val();
    var slice =  academy.slice(-2);
    var limit =  slice.charAt(0);
    
    console.log(limit);
    // if(academy == 1){
    //     var limit = 2;
    // }else if(academy == 2){
    //     var limit = 2;
    // }else if(academy == 3){
    //     var limit = 4;
    // }
    
    
    $('input[name="num_days[]"]').on('change', function(evt) {
       if($(this).siblings(':checked').length >= limit) {
           this.checked = false;
           alert('you can select maximum '+limit+' training times!');
       }
    });
    
    
    
    $('#cost').hide();
    $('#btn-sbmt').hide();
    $('#btn-reset').hide();
    
    $("#sport").change(function(){
        if (window.location.href.indexOf("client/check/courts") > -1) {
            $('#hidden-btn').click();
            return false;
        }
        sportImages();
    })
    
    /*On Load set sport images*/    
    sportImages();

    function sportImages(){
        let sport_value = $("#sport").val();
        
        if(typeof sport_value !== 'undefined'){
            if(sport_value === ""){
                //$(".sport-detail").html("");
            }
            else if(sport_value == "1"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .football-images").addClass('show');
            }
            else if(sport_value == "2"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .tennis-images").addClass('show');
            }
            else if(sport_value == "5"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .athletics-images").addClass('show');
            }
            else{
                //$(".sport-detail").html("");
            }   
        }    
    }



    var dateToday = new Date();
    $( "#date" ).datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0
    });

    

    })

</script>


<script>


    $('#btnCalculate').click(function() {
        
        var numberOfChecked = $('input:checkbox:checked').length;
        
        $('#day_num_count').val(numberOfChecked);
        
        console.log(numberOfChecked);
        
        // $('input[name="num_days"]').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        
        if($('input[name="start_date"]').val() == '')
        {
            alert('please select date!');
        } 
        // else if($('input[name="cls"]:checked').length == 0){
        //     alert('please select training time!');
        // } 
        else if($('input[name="num_days[]"]:checked').length == 0){
            alert('please training time!');
            // alert('please select days!');
        }
        
        else{


        $.ajax({
            type: 'POST',
            url: "{{ route('getPrice') }}",
            data: {
                sport: $('#sport').val(),
                 package: $('#my_checkbox').val(),
                  name: $('#childname').val(),
                   age: $('#age').val(),
                    terms: $('#terms').val(),
                     num_day: numberOfChecked,
                      startDate: $('input[name="start_date"]').val(),
                    
            },
            success: function(data) {
                
                console.log(data);
                
                
                 var  res = JSON.parse(data);
                 
                 
                 console.log(res.status)
                
                    if(res.status == 200){
                        
                        $('#cost').show();
                        $('#btn-sbmt').show();
                        $('#btn-reset').show();
                        
                        $('#cost-amnt').html(res.cost);
                        $('#total-amnt').val(res.cost);
                        
                    }
                    
                    $('#btnCalculate').hide();
            },
            error: function(data) {
                console.log(data);
            }
        });
        
        }
        
        
    });
    
    $('#btn-reset').click(function() {
        
        $('input[name="start_date"]').val('');
        // $('input[name="cls"]:checked').prop('checked', false);
        $('input[name="num_days[]"]:checked').prop('checked', false);
        $('#cost').hide();
        $('#cost-amnt').html('');
        $('#total-amnt').val('');
        
        $('#btn-sbmt').hide();
        $('#btn-reset').hide();
        $('#btnCalculate').show();
    });
    
    
</script>



    <!-- /.container-fluid -->
   

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script> --}}

<script type="text/javascript">
        

$(function() {
    $('#time').timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        minTime: '05:30am',
        maxTime: '11:30pm',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});

</script>

@endsection