@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">REVIEW BOOKING</h2>

            <p class="maindesc --big">
                Please review your selection and confirm by clicking on the button to proceed to payment
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    <table class="table --schedule-table --review-table athletics text-left" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                            <th>Id</th>
                            <th>Sport</th>
                            <th>Pitch</th>
                            <th style="width: 180px">Time</th>
                            <th>Products*Qty</th>
                            <th>Rate</th>
                            <th>Total</th>
                        </thead>

                        <tbody>
                           
                            @foreach($details as $key=> $detail)

                            <tr>
                                <td>
                                    {{$booking_id = $detail['booking_id']}}<br>
                                </td>
                                <td>
                                    {{$sports = $detail['sports']}}<br>
                                </td>
                                <td>
                                    {{$pitch = $detail['pitch']}}<br>
                                </td>
                                <td>
                                    From: {{$detail['from']}}<br>
                                    To: {{$detail['to']}}
                                </td>
                                <td>
                                    @if(count($detail['name']) !==0)
                                       @foreach($detail['name'] as $key=> $productName)
                                       {{$productName}}
                                       <br>
                                       @endforeach
                                    @endif
                                </td>
                                <td>
                                   
                                  Price: {{$detail['court_rate']}}<br>
                                  Product Price: {{$detail['product_total']}}<br>
                                   
                                </td>
                                <td>
                                   
                                  {{$totalAmount = $detail['total']}}<br>
                                  <input type="hidden" value="{{$client_id = $detail['client_id']}}" >
                                </td>
                            </tr>
                            
                            @endforeach
                           
                            
                        </tbody>
                    </table>
                      <div class='default-form'>
                    
                    <div class='control-group'>
                        
                        <select name='client_id' id='client_id' class='form-field' required> 
                    
                        <option selected="selected" disabled value=''>Select Card</option> 
                    
                        </select>
                    </div>
                    
                    

                        <div id="pagerender"></div>
                     

                </div>
            </div>
        </div>

    </div>
</section>

<script>
    
    
$(document).ready(function(){
    
    console.log('test');
    
     var values = '{{ auth()->user()->id }}';

    $.ajax({
        url: "{{ route('getMerchantDetails') }}",
        type: "get",
        data: { values:values } ,
        dataType:"json",
        success: function (response) {
            
                console.log(response.success);

          // You will get response from your PHP page (what you echo or print)
        //  
          
          if(response.success == 200){
              
                            var obj = response.data;
                            // console.log(obj);
                            obj.forEach(function(element) {

                                console.log(element);
                                $('#client_id').append('<option value="' + element.id + '">'+ element.card_number +'</option>');
                            });
                            $('#client_id').append('<option value="">Add new card</option>');
              
              
          }else{
               $('#client_id').append('<option value="">Add new card</option>');
          }
          
                
          
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
    });
    
    
    
    $('#client_id').on('change',function(){
        
        
        
        var get_id = $(this).val(); 
        
        var token_dyn = $('#token_dyn').val();
    
    console.log(get_id);    
        
         $.ajax({
        url: "{{ route('getMerchantToken') }}",
        type: "get",
        data: { token_name:'{{ $token_name }}','client_id':{{ $client_id }},'booking_id':{{ $booking_id }},'sports':'{{ $sports }}','pitch':'{{ $pitch }}','totalAmount':{{ $totalAmount }},values:get_id } ,
        dataType:"json",
        success: function (response) {
            
            $('#pagerender').empty();
            
                console.log(response);
          
              $('#pagerender').html(response.html);
              
              
              
              console.log(response.data);
              
              $('#token_name').val(response.data);
              
         
          
                
          
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(textStatus, errorThrown);
        }
    });
        
    });
   
    
    
    
});
    
    
</script>


    
     
<!-- /.container-fluid -->
@endsection