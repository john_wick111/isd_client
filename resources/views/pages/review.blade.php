@extends('layouts.user')

<style>
	.total-states {
		/*display: block;*/
		/* text-align: right; */
		font-weight: bolder;
		/*display: flex;*/
		/*align-content: space-around;*/
		/*flex-di/rection: row;*/
		/*align-items/: center;*/
		/*flex-wrap: nowrap;*/
		/*justify-content: space-between;*/
		text-align: right;
	}

	.wallet-checkout {
		text-align: left;
		display: flex;
		justify-content: space-between;
		flex-direction: column;
		flex-wrap: wrap;
		align-content: flex-end;
		list-style: none;
	}
</style>

@section('content')
<!-- Begin Page Content -->

<section class="aboutpage-section --sports-section">
	<div class="container-wrapper">

		<div class="mb-40">
			<h2 class="maintitle">REVIEW BOOKING</h2>

			<p class="maindesc --big">
				Please review your selection and confirm by clicking on the button to proceed to payment
			</p>
		</div>

		<div class="row">
			<div class="col-lg-3">
				@include('includes.sidebar-navigation')
			</div>

			<div class="col-lg-9">
				<div class="article right-side">

					<table class="table --schedule-table --review-table athletics text-left" id="dataTable1"
						width="100%" cellspacing="0">
						<thead>
							<th>Id</th>
							<th>Sport</th>
							<th>Pitch</th>
							<th style="width: 180px">Time</th>
							<th>Products*Qty</th>
							<th>Rate</th>
							<th>Total</th>
						</thead>

						<tbody>

							@foreach ($details as $key => $detail)
							<tr>
								<td>
									{{ $booking_id = $detail['booking_id'] }}<br>
								</td>
								<td>
									{{ $sports = $detail['sports'] }}<br>
								</td>
								<td>
									{{ $pitch = $detail['pitch'] }}<br>
								</td>
								<td>
									From: {{ $detail['from'] }}<br>
									To: {{ $detail['to'] }}
								</td>
								<td>
									@if (count($detail['name']) !== 0)
									@foreach ($detail['name'] as $key => $productName)
									{{ $productName }}
									<br>
									@endforeach
									@endif
								</td>
								<td>

									Price: {{ $detail['court_rate'] }}<br>
									Product Price: {{ $detail['product_total'] }}<br>

								</td>
								<td>

									{{ $totalAmount = $detail['total'] }}<br>
									<input type="hidden" value="{{ $client_id = $detail['client_id'] }}">
								</td>
							</tr>
							@endforeach


						</tbody>
					</table>



					<div class='default-form'>



						<div class='control-group'>

							<select name='client_id' id='client_id' class='form-field' required>

								<option selected="selected" disabled value=''>Select Card</option>

							</select>

						</div>





					</div>

					<div class="row">
						<div class="col-md-6">

							<div class="control-group">
								<div class="form-check form-check-inline">
									<label class="form-check-label form-label">
										<input class="" type="checkbox" name="is_wallet" id="use-wallet" value="1"> Use
										wallet </label>
								</div>
							</div>

							<input type="hidden" id="updatedTotalAmount" value="{{ $totalAmount }}" />
							<input type="hidden" id="walletAmount" value="" />


							<div class="control-group" id="wallet-complete-cta">

							</div>

							<br>

							<div id="pagerender"></div>

						</div>
					
						<div class="col-md-6">

							<div class="control-group total-states" id='update-calculation'>

								<div class="default-form">

									<ul class="wallet-checkout">
										<li>
											<label class="form-label">Sub Total : </label><span
												id='sub-total'></span><br>

										</li>
										<li>
											<label class="form-label">Wallet Amount : </label><span
												id='wallet-amnt'></span><br>

										</li>

										<li>
											<label class="form-label">Net Amount : </label><span
												id='net-amnt'></span><br>

										</li>


									</ul>

									{{-- <label class="form-label">Wallet Amount : </label><span id='wallet-amnt'></span><br>
									<label class="form-label">Net Amount : </label><span id='net-amnt'></span> --}}

								</div>

							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
</section>

<script>
	$(document).ready(function() {

	  $('#update-calculation').hide();

	  $.ajaxSetup({
	   headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	   }
	  });

	  console.log('test');

	  var values = '{{ auth()->user()->id }}';

	  $.ajax({
	   url: "{{ route('getMerchantDetails') }}",
	   type: "get",
	   data: {
	    values: values
	   },
	   dataType: "json",
	   success: function(response) {

	    console.log(response.success);

	    // You will get response from your PHP page (what you echo or print)
	    //  

	    if (response.success == 200) {

	     var obj = response.data;
	     // console.log(obj);
	     obj.forEach(function(element) {

	      console.log(element);
	      $('#client_id').append('<option value="' + element.id + '">' + element.card_number + '</option>');
	     });
	     $('#client_id').append('<option value="">Add new card</option>');


	    } else {
	     $('#client_id').append('<option value="">Add new card</option>');
	    }



	   },
	   error: function(jqXHR, textStatus, errorThrown) {
	    console.log(textStatus, errorThrown);
	   }
	  });




	  $('#use-wallet').change(function() {

	   var wallet = $(this).is(':checked');

	   $('#client_id').prop('selectedIndex', 0);

	   // $('#textbox1').val($(this).is(':checked'));
	   if (wallet) {

	    $.ajax({
	     url: "{{ route('processWallet') }}",
	     type: "post",
	     data: {
	      'client_id': {{ $client_id }},
	      'totalAmount': {{ $totalAmount }},
	     },
	     dataType: "json",
	     success: function(response) {

	      console.log(response);

	      // wallet exists
	      if (response.success == 200) {

	       console.log(response.message);

	       $('#updatedTotalAmount').val(response.data['finalAmount']);
	       $('#walletAmount').val(response.data['lessWalletAmount']);


	       $('#sub-total').html({{ $totalAmount }});
	       $('#wallet-amnt').html(response.data['lessWalletAmount']);
	       $('#net-amnt').html(response.data['finalAmount']);
	       // 			wallet-amnt
	       // 			net-amnt

	       $('#update-calculation').show();



	       if (response.data['finalAmount'] == 0) {

	        $('#wallet-complete-cta').html(
	         '<input type="hidden" id="less-wallet-amount"/> <input type="hidden" id="booking_id"/> <button type="button" class="btn --btn-primary" id="wallet">Pay Now</button>'
	         );

	        $('#less-wallet-amount').val(response.data['lessWalletAmount']);

	       }
	       $('#booking_id').val({{ $booking_id }});


	      }
	      // wallet doesn't exists 
	      else if (response.success == 500) {

	       console.log(response.message);

	       $('#wallet-complete-cta').html('');
	       $('#update-calculation').hide();

	      }

	     },
	     error: function(jqXHR, textStatus, errorThrown) {
	      console.log(textStatus, errorThrown);
	     }
	    });





	   } else {
	    // alert('unchecked');
	    $('#wallet-complete-cta').html('');
		$('#pagerender').empty();
	    $('#update-calculation').hide();

	    $('#updatedTotalAmount').val({{ $totalAmount }});

	   }

	  });





	  $('#client_id').on('change', function() {

	   var wallet = $('#use-wallet').is(':checked');

	   var get_id = $(this).val();

	   var token_dyn = $('#token_dyn').val();

	   console.log(get_id);

	   $.ajax({
	    url: "{{ route('getMerchantToken') }}",
	    type: "get",
	    data: {
	     token_name: '{{ $token_name }}',
	     'client_id': {{ $client_id }},
	     'booking_id': {{ $booking_id }},
	     'sports': '{{ $sports }}',
	     'pitch': '{{ $pitch }}',
	     'totalAmount': $('#updatedTotalAmount').val(),
	     'walletAmount': $('#walletAmount').val(),
	     values: get_id
	    },
	    dataType: "json",
	    success: function(response) {

	     $('#pagerender').empty();

	     console.log(response);

	     $('#pagerender').html(response.html);



	     console.log(response.data);

	     $('#token_name').val(response.data);

	    },
	    error: function(jqXHR, textStatus, errorThrown) {
	     console.log(textStatus, errorThrown);
	    }
	   });

	  });




	  $(document).on('click', '#wallet', function() {

	   $.ajax({
	    url: "{{ route('processWalletSuccess') }}",
	    type: "post",
	    data: {
	     'lessWalletAmount': $('#less-wallet-amount').val(),
	     'booking_id': $('#booking_id').val(),
	     'booking_amount': {{ $totalAmount }},
	    },
	    dataType: "json",
	    success: function(response) {

	     console.log(response);

	     //  console.log('{{ route('wallet.booking.thankyou', ['booking_id' => $booking_id, 'booking_amount', $totalAmount]) }}');

	     window.location.assign(
	      "{{ route('wallet.booking.thankyou', ['booking_id' => $booking_id, 'booking_amount' => $totalAmount]) }}"
	      );

	     //   route('thankyou', );

	    },
	    error: function(jqXHR, textStatus, errorThrown) {
	     console.log(textStatus, errorThrown);
	    }
	   });

	  });

	 });
</script>




<!-- /.container-fluid -->
@endsection