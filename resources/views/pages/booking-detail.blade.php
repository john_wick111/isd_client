@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">REVIEW BOOKING</h2>

            <p class="maindesc --big">
                Please review your selection and confirm by clicking on the button to proceed to payment
            </p>
        </div>

        <div class="article right-side">

            <div class="table-responsive">

                <table class="table --schedule-table coaches text-left" id="all-bookings" width="100%" cellspacing="0">
                
                    @foreach($booking_details as $key=> $detail)
                        <tr>
                            <th>Booking #</th>
                            <td>
                                    {{$detail->id}}<br>
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Status</th>
                            <td>
                            
                                @if($detail->status == 0)         
                                      Unpaid         
                                @else
                                      Paid        
                                @endif
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Date</th>
                            <td>
                            
                                {{$detail->date}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>From</th>
                            <td>
                            
                                {{$detail->from}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>To</th>
                            <td>
                            
                                {{$detail->to}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Pitch</th>
                            <td>
                            
                                {{$detail->pitch}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Size</th>
                            <td>
                            
                                {{$detail->size}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Price</th>
                            <td>
                            
                                {{$detail->price}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Discount</th>
                            <td>
                            
                                {{$detail->dicsount}}
                        
                            </td>
                        </tr>
                        
                        <tr>
                            <th>Total</th>
                            <td>
                            
                                {{$detail->total}}
                        
                            </td>
                        </tr>
                    @endforeach
                    
                </table>
                
                <h6 class="font-weight-bold text-primary mb-10">Products</h6>
                
                <table class="table --schedule-table --review-table athletics text-left" id="all-bookings" width="100%" cellspacing="0">
                    <tr>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Date</th>
                        <th>Qty</th>
                    </tr>
                       
                    @foreach($product_details as $key=> $detail)

                    <tr>
                        <td>
                            {{$detail->title}}<br>
                        </td>
                        <td>
                            {{$detail->price}}<br>
                        </td>
                        <td>
                            {{$detail->total}}<br>
                        </td>
                        <td>
                            {{$detail->created_at}}
                        </td>
                        <td>
                            {{$detail->count}}
                        </td>
                    </tr>

                    @endforeach

                </table>

            </div>

        </div>

    </div>
</section>


@endsection