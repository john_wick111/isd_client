@extends('layouts.user')
@section('content')
<style>
    /*.ui-datepicker-calendar {*/
    /*display: none;*/
    /*}*/

    /*#hide{*/
    /*    display:none;*/
    /*}*/
</style>

<!-- Begin Page Content -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
{{--
<link rel="stylesheet"
    href="https://frontendscript.com/demo/jquery-timepicker-24-hour-format/dist/wickedpicker.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet" /> --}}

{{-- <script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script> --}}

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">Book your session at ISD!</h2>

            <p class="maindesc --big">
                Please choose your sport, pitch or court, booking day, time & duration, from the drop-down menu.
                <br>
                If you have any questions, please call us on 04 448 1555 and we will more than happy to help you.
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    {!! Form::open(['route' => 'availableClasses', 'files' => 'true', 'method' => 'POST',
                    'enctype'=>"multipart/form-data", 'class'=>'default-form']) !!}

                    <!-- Default Card Example -->

                    @if(!empty($error_status) && $error_status==1)
                    <div class="alert --danger" role="alert">
                        <strong>Oops!</strong> No Courts is Found
                    </div>
                    @endif


                    <div class="row">
                        <div class="col-md-12">

                            <div class="control-group">
                                {!! Form::label('cities', 'City Name', ['class' => 'form-label']) !!}
                                <select id="cities" class="form-field" name="cities" required autofocus>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ isset($request->cities) && $request->cities ==
                                        $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="control-group">
                                {!! Form::label('academy', 'Academy Name', ['class' => 'form-label']) !!}
                                <select id="academy" class="form-field" name="academy" required autofocus>
                                    @foreach($academies as $academy)
                                    <option value="{{ $academy->id }}" {{ isset($request->academy) && $request->academy
                                        == $academy->id ? 'selected' : '' }}>{{ $academy->title }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="control-group">
                                {!! Form::label('childname', 'Child Name', ['class' => 'form-label']) !!}
                                <select id="childname" class="form-field select2" name="childname"></select>
                                {{-- <input id="childname" type="text" class="form-field " name="childname" value=""
                                    required placeholder="Child Name" autofocus> --}}
                            </div>

                            <div class="control-group">
                                {!! Form::label('dob', 'Date of Birth', ['class' => 'form-label']) !!}
                                {{ Form::text('dob',$request->date ?? null, ['class' => 'form-field
                                age','id'=>'date','autocomplete'=>"off",'required'=>'required']) }}
                            </div>

                            <input type="hidden" id="age_category" name="age_category" value="" />
                            <input type="hidden" id="package_name" name="package_name" value="" />

                            <div class="control-group">

                                <select id="term" class="form-field select2" multiple="multiple" name="term"
                                    required></select>

                            </div>


                        </div>




                        <div class="col-md-12 term" id="hide">
                            <b>{!! Form::label('package_name', 'Packages Name', ['class' => 'form-label']) !!}</b>
                            <div class="control-group">
                            </div>
                        </div>
                        <input type="hidden" id="terms" name="terms" value="" />
                        <input type="hidden" id="sport" name="sport" value="" />


                    </div>


                    <div class="control-group">
                        <button type="submit" class="btn --btn-primary" name="submit" id="submit"
                            value="1">NEXT</button>
                        @if(count($data)!=0)
                        <button type="submit" class="btn --football fc-white" name="submit" value="2"
                            id="proceed">Proceed</button>
                        @endif
                    </div>

                    {!! Form::close() !!}

                    <form method="post" action="{{route('client.reset.form')}}" id="hiddenForm"
                        enctype="multipart/form-data" hidden>

                        <input type="hidden" value="{{$request->sport ?? null}}" name="sport">
                        <input type="hidden" value="{{$request->time ?? null}}" name="time">
                        <input type="hidden" value="{{$request->pitch ?? null}}" name="pitch">
                        <input type="hidden" value="{{$request->date ?? null}}" name="date">
                        <input type="hidden" value="{{$request->duration ?? null}}" name="duration">

                        <input type="submit" value="submit" name="submit" id="hidden-btn">
                    </form>

                    <div class="sport-detail">
                        <div class="football-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Indoor Artificial</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg')}}'
                                            alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Outdoor Artificial</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg')}}'
                                            alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Outdoor Grass</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg')}}'
                                            alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="tennis-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 1</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg')}}'
                                            alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 2</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg')}}'
                                            alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 3</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg')}}'
                                            alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="athletics-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Athletics Track</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg"
                                        data-fancybox>
                                        <img src='{{ asset('
                                            https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg')}}'
                                            alt='athletics-sport' class='img-fluid'>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>

<script>
    $(document).ready(function(){
    
            $('#childname').select2({tags: true});
            termss = []
            AvilableDays = []
            var package = 0;
            var x = 0;

     
      
            $( window ).load(function() {

                var id = {{ Auth::user()->id }};
                var data = {id : id}
                $.ajax({
                    url: '{{ "https://bookings.isddubai.com/api/v1/user/show" }}',
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {

                        // age
                        $('.age').val(response['data']['children'][0].date);
                     
                        
                        $.each(response['data']['children'], function(key, value) {   
                            $('#childname').append($("<option></option>").attr("value", value.id).text(value.name)); 
                        });                          
                       
                    }
                }); 
                
                
                $.ajax({
                    url: '{{ "https://bookings.isddubai.com/api/v1/academy/terms" }}',
                    type: "GET",
                    dataType: 'json',
                    success: function(response) {
                        
                        console.log(response);

                        // age
                        // $('.age').val(response['data']['children'][0].date);
                     
                        
                        $.each(response['data'], function(key, value) {   
                            $('#term').append($("<option></option>").attr("value", value.id).text(value.name)); 
                        });                          
                       
                    }
                }); 
          
           
            });
            
            //
            $("#childname").change(function() {
                var id = $(this).find('option:selected').val();
                $.ajax({
                    url: '{{ "https://bookings.isddubai.com/api/v1/user/show" }}',
                    type: "POST",
                    data: {id:id},
                    dataType: 'json',
                    success: function(response) {
                    //   console.log(response.data.date);
                       
                        // let current_datetime = new Date("2008-10-08");

                        // let formatted_date = String(current_datetime.getMonth()).padStart(2, '0') + "/" + String(current_datetime.getDate()).padStart(2, '0') + "/" + current_datetime.getFullYear();
                        // console.log(formatted_date);

                        // age
                        $('.age').val(response.data.date);
                        $('.term .control-group').empty();
                     
                       
                        // let current_datetime = new Date(['data']['children'][0].date);

                        // let formatted_date = String(current_datetime.getMonth()).padStart(2, '0') + "/" + String(current_datetime.getDate()).padStart(2, '0') + "/" + current_datetime.getFullYear();
                        // console.log(formatted_date);

                       
                    }
                }); 
                
            });
            
            
            
            $('#term').select2();
            $("#term").change(function() {
                
                var year = $("#date").val();
                
                console.log(year);
                
                var academy = $("#academy").val();
                dob = new Date(year);
                
                var today = new Date();
                var my_age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));  
                
                console.log(my_age);
                
                $('#term').each( function () {
                    termss.push($(this).val());
                });
                
                var data = {
                    years : my_age,
                    terms : termss[0],
                    academy : $("#academy").val(),
                    city : $("#cities").val(),
                }
                
                console.log(data);
                
                $.ajax({
                    url: '{{ "https://bookings.isddubai.com/api/v1/academy/packages/check" }}',
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        
                        $('.term .control-group').empty();
                        
                        console.log(response);
                        
                        console.log(response['data']);
                        if(response['data'] == null){
                            alert("please select another date and terms");
                            return;
                        }else{
                            console.log(response['data']);
      
                            // $.each(response['classes'], function(key, value) {   
                            //     $('#select_classes').append($("<option></option>").attr("value", value.id).text(value.title)); 
                            // }); 

                            var i = 0;
                            $.each(response['data'], function(key, value) {  
                                i++;
                                if(i == 1){
                                    package = value.id
                                    console.log(value);
                                    $('.term .control-group').append('<input type="radio" id="'+ value.id +'" name="my_checkbox" value="'+ value.id +'" data-package_name="'+value.package_name+'" data-age-group="'+value.age_group+'" data-terms='+value.terms+' data-sport="'+value.sport+'" required/><label for="' + value.id + '"> '+value.package_name+'<label/><br>');
                                    if(value.day1_active == 1){
                                        AvilableDays.push('1')
                                    }
                                    if(value.day2_active == 1){
                                        AvilableDays.push('2')
                                    }
                                    if(value.day3_active == 1){
                                        this.day3_active = true
                                        AvilableDays.push('3')
                                    }
                                    if(value.day4_active == 1){
                                        this.day4_active = true
                                        AvilableDays.push('4')
                                    }
                                    if(value.day5_active == 1){
                                        this.day5_active = true
                                        AvilableDays.push('5')
                                    }
                                    if(value.day6_active == 1){
                                        this.day6_active = true
                                        AvilableDays.push('6')
                                    }
                                    if(value.day7_active == 1){
                                        this.day7_active = true
                                        AvilableDays.push('7')
                                    }                                    
                                }else{
                                    // $('#packageName').append($("<option></option>").attr("value", value.id).text(value.package_name)); 
                                    $('.term .control-group').append('<input type="radio" id="'+ value.id +'" name="my_checkbox" value="'+ value.id +'" data-package_name="'+value.package_name+'" data-age-group="'+value.age_group+'" data-terms="'+value.terms+'" data-sport="'+value.sport+'" required/><label for="' + value.id + '"> '+value.package_name+'<label/><i<br>');
                                    
                                }
                            }); 

    
                            if(AvilableDays.length == 1){
                               x = AvilableDays[0];
                            }else{
                                // $('.type_num_days').removeClass("hidden");
                                // $.each(AvilableDays, function(key, value) {   
                                //     $('#type_num_days').append($("<option></option>").attr("value", value).text(value)); 
                                // });                                
                            }
                        } 

                    }
                });                


            });
            
            
   $(document).on('change','input[name="my_checkbox"]',function() {
    
            var package_name = $("input[name='my_checkbox']:checked").data('package_name');
                var age_group = $("input[name='my_checkbox']:checked").data('age-group');
                const terms = $("input[name='my_checkbox']:checked").data('terms');
                var sport = $("input[name='my_checkbox']:checked").data('sport');

                const hello = '['+terms+']';

                // const myvariable = "great";

                // const favePoem = "My text is."+myvariable+".";

                // console.log(terms);

                $('#package_name').val(package_name);

                $('#age_category').val(age_group);
                $('#terms').val(hello);
                $('#sport').val(sport);


        });


    
      
      
      
      
      
      //////////////////////////////////////////////////////
      

    $("#sport").change(function(){
        if (window.location.href.indexOf("client/check/courts") > -1) {
            $('#hidden-btn').click();
            return false;
        }
        sportImages();
    })
    
    /*On Load set sport images*/    
    sportImages();

    function sportImages(){
        let sport_value = $("#sport").val();
        
        if(typeof sport_value !== 'undefined'){
            if(sport_value === ""){
                //$(".sport-detail").html("");
            }
            else if(sport_value == "1"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .football-images").addClass('show');
            }
            else if(sport_value == "2"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .tennis-images").addClass('show');
            }
            else if(sport_value == "5"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .athletics-images").addClass('show');
            }
            else{
                //$(".sport-detail").html("");
            }   
        }    
    }



    var dateToday = new Date();
    $( "#date" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/dd/yy',
        yearRange: "2002:2018",
        // defaultDate: "01/01/2002",
        showButtonPanel: true,
         
    });
    

    })

</script>


<script>
    // $(document).on('change','input[name="my_checkbox"]',function() {
    
    //     var package_name = $("input[name='my_checkbox']:checked").data('package_name');
    //     $('#package_name').val(package_name);
    
    // });

    


    // $('#date').change(function() {
    //     // var gender = $("input[name=gender]:checked").val();
        
        
        
    //     var cities = $("select#cities option").filter(":selected").val();
    //     var academy = $("select#academy option").filter(":selected").val();
    //     var date = $('#date').val();
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
        
    //     dob = new Date(date);
    //     var today = new Date();
    //     var my_age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
        
    //     console.log(my_age);


    //     $.ajax({
    //         type: 'GET',
    //         url: "{{ route('getCategory') }}",
    //         data: {
    //             cities: cities,
    //             academy: academy,
    //             date: date,
    //         },
    //         dataType: 'json',
    //         success: function(data) {
                
    //             console.log(data.categoryId);
                
    //             $('#age_category').val(data.categoryId);
                
    //             $('.term .control-group').empty();
                
    //             if(data.dataResult == ''){
    //                 // console.log('emptyy hai');
            
    //                 $('.term .control-group').html('no data found!');
            
    //                 $('#submit').attr('disabled',true);        
                    
    //             }else{
    //                 console.log('data found');
                    
    //                 $('#submit').removeAttr("disabled");
                    
    //             }
    //              $('.term').show();
    //              var obj = data.dataResult;
                 
    //              $('#terms').val(obj[0]['terms']);
    //              $('#sport').val(obj[0]['sport']);
                 
    //              obj.forEach(function(element) {
    //                  $('.term .control-group').append('<input type="radio" id="'+ element.id +'" name="my_checkbox" value="'+ element.id +'" data-package_name="'+element.package_name+'" required/><label for="' + element.id + '"> '+element.package_name+'<label/><br>');
    //             });
                

    //         },
    //         error: function(data) {
    //             console.log(data);
    //         }
    //     });
    // });
    
    
</script>

<!-- /.container-fluid -->


{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js">
</script> --}}

<script type="text/javascript">
    $(function() {
    $('#time').timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        minTime: '05:30am',
        maxTime: '11:30pm',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});

</script>

@endsection