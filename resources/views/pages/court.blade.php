@extends('layouts.user')
@section('content')

<!-- Begin Page Content -->

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">Book your session at ISD!</h2>

            <p class="maindesc --big">
                Please choose your sport, pitch or court, booking day, time & duration, from the drop-down menu.
                <br>
                If you have any questions, please call us on 04 448 1555 and we will more than happy to help you.
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    {!! Form::open(['route' => 'client.bookings.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'id'=>'courts', 'class'=>'default-form']) !!}
                                    
                        <div class="row">
                            <div class="col-md-6">
                                <p class="fs-medium mb-10">
                                    <strong>Sport: </strong>
                                    {{Session::get('sport_name')}}
                                </p>
                                <p class="fs-medium mb-10">
                                    <strong>Pitch: </strong>
                                    {{Session::get('pitch_name')}}
                                </p>
                                <p class="fs-medium mb-10">
                                    <strong>Date: </strong>
                                    {{Session::get('date')}}
                                </p>
                                
                            </div>
                            <div class="col-md-6">
                                <p class="fs-medium mb-10">
                                    <strong>From:</strong>
                                    {{Session::get('from')}}
                                </p>
                                <p class="fs-medium mb-10">
                                    <strong>To: </strong>
                                    {{Session::get('to')}}
                                </p>
                            </div>
                        </div>

                        <p class="h5 fw-bold fc-primary mb-10 mt-30">Select Products</p>

                                <div class="mb-4">
                                        
                                    <table class="table --bordered text-left" id="dataTable1" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                
                                                <th>Product Name</th>
                                                <th>Rate</th>
                                                <th>Quantity</th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            @foreach($products as $key=> $product)
                                            <tr>
                                                
                                                <td style="vertical-align: middle;">
                                                    <p class="maindesc">{{$product->title}}</p>
                                                </td>
                                                    
                                                <td style="vertical-align: middle;">
                                                    <p class="maindesc">{{$product->price}}</p>
                                                    <input type="hidden" name="products[]" class="form-control" value="{{$product->id}}">
                                                </td>

                                                <td style="width: 140px">
                                                    <div class="control-group mb-0">
                                                        <input type="number" name="qty[]" class="form-field --small">
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        
                                    </table>

                                    <div class="control-group">
                                        
                                        <a href="{{route('client.bookings')}}" class="btn --football">
                                            <span class="fc-white">Cancel</span>
                                        </a>

                                        <button type="submit" class="btn --btn-primary" id="proceed">Proceed</button>
                                    </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                </div>
            </div>
        </div>

    </div>
</section>

    
@endsection