<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Password Reset - ISD</title>

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;600;700&display=swap" rel="stylesheet">

</head>
<body style="background: #ffffff; margin: 0; padding: 0; font-family: Arial, sans-serif; font-size: 18px; line-height: 1.6; color: #3e2b64;">
	<div style="margin: 0 auto; padding: 0px; width: 600px; max-width: 600px;">
		<table cellpadding="0" style="width: 600px; background: #ffffff; border-collapse: collapse; border-spacing: 0; max-width: 600px; margin: 0 auto;">
			<tbody>

				<tr>
					<td colspan="2">
						<a href="https://isddubai.com/" target="_blank" style="display: block;">
							<img src="http://www.isddubai.com/emails/app-emails/password-reset/images/header.png" alt="ISD Dubai" style="display: block; margin: 0 auto;">
						</a>
					</td>
				</tr>

				<tr>
					<td colspan="2" style="padding: 20px 0;">

						<p style="margin: 0 60px 20px; line-height: 1.4; font-size: 16px; font-family: Arial, sans-serif; color: #3e2b64;">

							You are receiving this email because you requested your password to be reset. Please use this verification code to reset your password in the App
							<br><br>

							<strong>{{$details['code']}}</strong> <br><br>

							If you didn’t request your password to be reset, please ignore this email.  Your password will not be changed

						</p>

						<p style="margin: 0 60px 0px; line-height: 1.4; font-size: 20px; font-family: 'Oswald', Arial, sans-serif; font-weight: 700; color: #3e2b64; text-transform: uppercase;"> 

							isddubai.com

						</p>

					</td>
				</tr>

				<tr>

					<td>

						<img src="http://www.isddubai.com/emails/app-emails/password-reset/images/footer.png" alt="ISD Dubai" style="display: block; margin: 0 auto;">

					</td>
				</tr>

			</tbody>
		</table>
	</div>	
</body>
</html>