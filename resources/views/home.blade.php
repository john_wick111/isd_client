<?php

    // $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";

    $path = $_SERVER["DOCUMENT_ROOT"];

    $title = "Home Page";

    $keywords = "";

    $desc = "";

    $pageclass = "homepg";

?>


@extends('layouts.layout')
@section('content')



<!-- Hero Banner -->

<section class="hero-banner" style="background-image: url('/assets-web/images/homepage-banner.webp');">

    <div class="container-wrapper">

        <div class="inner-content">

            <h1 class="title">

                <strong>inspiratus sports district</strong> <br />

                compete with yourself

            </h1>

        </div>

    </div>

</section>



<!-- About Section -->

<section class="sec-padding">

    <div class="container-wrapper">

        <h2 class="maintitle text-center">world class facility</h2>

        <p class="maindesc --big text-center">

            Home to world-class multi-sports academies, playing venues, entertainment parks and much more, ISD Dubai Sports City is transforming Dubai’s Sporting Scene. Located in the heart of Dubai, it is Dubai’s destination of choice for football, athletics, rugby, tennis and padel sports fans, providing athletic training, competition and entertainment, and a gateway to physical and mental wellness. At ISD Dubai Sports City, you are invited to compete with yourself. 

        </p>

    </div>

</section>



<!-- Divider -->

<hr class="divider" />



<!-- Choose your Sport -->

<section class="sec-padding">

    <div class="container-wrapper">

        <h2 class="maintitle text-center">choose your SPORT</h2>

        

        <div class="row">

            <div class="col-lg-4 col-md-6 scrollme">

                <div class="box --sports-box --without-image animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatex="-400">

                    <p class="text">ready</p>

                </div>

            </div>



            <div class="col-lg-4 col-md-6 scrollme">

                <div class="box --sports-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatey="400">



                    <picture class="imghvr-shutter-out-vert football">

                        <img src="/assets-web/images/gallery/football/thumbnail.webp" alt="Football Academy at ISD">

                        <figcaption>

                            <a href="https://ae.laligacademy.com/">LaLiga Academy</a>

                            <a href="https://ae.laligacademy.com/registration">Registration</a>

                            <a href="https://pitchbooking.isddubai.com/">Pitch Rental</a>

                        </figcaption>

                    </picture>



                    <p class="text">football</p>

                </div>

            </div>



            <div class="col-lg-4 col-md-6 scrollme">

                <div class="box --sports-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatex="400">



                    <picture class="imghvr-shutter-out-vert tennis">

                        <img src="/assets-web/images/gallery/tennis/thumbnail.webp" alt="Tennis Academy at ISD">

                        <figcaption>

                            <a href="/tennis">ISD Tennis Academy</a>

                            <a href="https://isddubai.com/venueHire">Venue Hire</a>

                        </figcaption>

                    </picture>



                    <p class="text">tennis</p>

                </div>

            </div>



            <div class="col-lg-4 col-md-6 scrollme">

                <div class="box --sports-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatex="-400">



                    <picture class="imghvr-shutter-out-vert rugby">

                        <img src="/assets-web/images/gallery/rugby/thumbnail.webp" alt="Rugby Academy at ISD">

                        <figcaption>

                            <a href="https://www.dkerugby.com/">DKE Rugby Club</a>

                            <a href="https://isddubai.com/venueHire">Venue Hire</a>

                        </figcaption>

                    </picture>



                    <p class="text">rugby</p>

                </div>

            </div>



            <div class="col-lg-4 col-md-6 scrollme">

                <div class="box --sports-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatey="400">



                    <picture class="imghvr-shutter-out-vert athletics">

                        <img src="/assets-web/images/gallery/athletics/thumbnail.webp" alt="Athletics Academy at ISD">

                        <figcaption>

                            <a href="/athletics">ISD Athletics Academy</a>

                            <a href="https://isddubai.com/venueHire">Venue Hire</a>

                        </figcaption>

                    </picture>



                    <p class="text">athletics</p>

                </div>

            </div>



            <div class="col-lg-4 col-md-6 scrollme">

                <div class="box --sports-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatex="400">



                    <picture class="imghvr-shutter-out-vert padel">

                        <img src="/assets-web/images/gallery/padel/thumbnail.webp" alt="Padel Academy at ISD">

                        <figcaption>

                            <a href="#">Coming Soon</a>

                        </figcaption>

                    </picture>



                    <p class="text">padel</p>

                </div>

            </div>



        </div>

    </div>

</section>



<!-- Divider -->

<hr class="divider" />



<!-- Partners -->

<section class="sec-padding">

    <div class="container-wrapper">

        <h1 class="maintitle text-center">world class partners</h1>

        <p class="maindesc --big text-center">

            We are proud to work with the world’s leading sports, entertainment and wellness brands. 

        </p>



        <div class="partners-section scrollme">

            <div class="box --partner-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/logos/footlab.svg" alt="Footlab" class="m-auto">

                </picture>

            </div>



            <div class="box --partner-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/logos/eupepsia.svg" alt="Eupepsia Sports Science and Wellness" class="m-auto">

                </picture>

            </div>



            <div class="box --partner-box animateme" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/logos/laliga.svg" alt="LaLiga Academy UAE" class="m-auto">

                </picture>

            </div>

        </div>

    </div>

</section>



<!-- Divider -->

<hr class="divider" />



<!-- Join an Academy -->

<section class="sec-padding join-academy-section">

    <div class="container-wrapper">

        <h1 class="maintitle text-center">join an academy</h1>



        <div class="academy-section scrollme">

            <a href="https://ae.laligacademy.com/registration" class="box --academy-box animateme" data-when="enter" data-from="0.2" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/icons/football.svg" alt="Football" class="m-auto">

                </picture>



                <p class="text">fooball</p>

            </a>



            <a href="/tennis" class="box --academy-box animateme" data-when="enter" data-from="0.4" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/icons/tennis.svg" alt="Tennis" class="m-auto">

                </picture>



                <p class="text">tennis</p>

            </a>



            <!-- <a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.6" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/icons/rugby.svg" alt="Rugby" class="m-auto">

                </picture>



                <p class="text">rugby</p>

            </a> -->



            <a href="/athletics" class="box --academy-box animateme" data-when="enter" data-from="0.8" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/icons/athletics.svg" alt="Athletics" class="m-auto">

                </picture>



                <p class="text">athletics</p>

            </a>



            <!-- <a href="#" class="box --academy-box animateme" data-when="enter" data-from="0.9" data-to="0" data-opacity="0" data-translatey="400">

                <picture>

                    <img src="/assets-web/images/icons/padel.svg" alt="Padel" class="m-auto">

                </picture>



                <p class="text">padel</p>

            </a> -->

        </div>



        <p class="maindesc --big text-center">

            Home to some of Dubai’s best academies including LaLiga Academy, ISD Athletics, ISD Tennis and ISD Rugby, ISD Dubai Sports City is championing sports development with international standard training programs that focus on whole player development, combining technical skills with mental agility and leadership qualities for players 5-18 years.  Academies also offer top athletes a pathway to professional sports careers and scholarships to universities in the US. 

        </p>



    </div>

</section>



<!-- Divider -->

<hr class="divider" />



<!-- Book a Pitch or Court -->

<section class="sec-padding">

    <div class="container-wrapper">

        <h2 class="maintitle text-center">book a pitch or court</h2>



        <div class="contact-cta">

            <!-- <a href="#" class="box --cta-box">

                <picture>

                    <img src="/assets-web/images/location-pin.png" alt="">

                </picture>

                <p class="text">venue</p>

            </a> -->



            <a href="#" class="box --cta-box">

                <picture>

                    <img src="/assets-web/images/icons/call.svg" alt="">

                </picture>

                <p class="text">‎+971 4 448 1555 <span class="fc-black">or</span> +971 52 519 3472</p>

            </a>



            <!-- <a href="#" class="box --cta-box">

                <picture>

                    <img src="/assets-web/images/book-now.png" alt="">

                </picture>

                <p class="text">book now</p>

            </a> -->

        </div>



        <p class="maindesc --big text-center">

            The facilities at ISD Dubai Sports City are designed, developed and maintained at certified professional standards. These include With 4 full-size FIFA standard and UAE FA approved football pitches, an indoor full-size 3G football pitch, 2 outdoor natural grass pitches, one outdoor astroturf pitch, 2 Rugby world standard pitches, including a 3,500-seat stadium, 1 Olympic standard 9-lane running track with high, long and triple jump facilities, 4 outdoor tennis courts, 8 indoor and outdoor World Padel Tour courts and the UAE’s only 2 indoor tennis and multisport courts. With outstanding grounds, changing rooms, meeting rooms and offices, ISD caters to all your sporting needs, ranging from a pitch booking, to tournament, league, customized event management. and corporate experiences.

        </p>



    </div>

</section>



<!-- Divider -->

<hr class="divider" />



<!-- My ISD Section -->

<section class="sec-padding">

    <div class="container-wrapper">

        <div class="row scrollme">

            <div class="col-lg-7 animateme" data-when="enter" data-from="0.50" data-to="0" data-opacity="0" data-translatex="-400">

                <h2 class="maintitle text-center">my isd</h2>

                <p class="maindesc --big text-center">

                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

                </p>



                <div class="download-application">

                    <picture class="">

                        <img src="/assets-web/images/icons/appleplay.svg" alt="">

                    </picture>



                    <picture class="">

                        <img src="/assets-web/images/icons/googleplay.svg" alt="">

                    </picture>

                </div>

            </div>



            <div class="col-xl-4 col-lg-5 offset-xl-1 animateme" data-when="enter" data-from="0.50" data-to="0" data-opacity="0" data-translatex="400">

                <picture class="mobile-icon">

                    <img src="/assets-web/images/mobile-icon.png" alt="ISD Mobile" class="m-auto">

                </picture>

            </div>

        </div>

    </div>

</section>
@endsection


