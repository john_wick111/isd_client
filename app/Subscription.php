<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Subscription extends Model
{
    public function academies()
    {
        return $this->belongsTo('App\Academy', 'academy');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'subscriber');
    }

    public function sport(){
         return $this->belongsToThrough('App\Academy', 'App\Sport');
    }
}
