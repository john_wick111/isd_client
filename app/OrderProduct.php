<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OrderProduct extends Model
{
    public function details()
    {
        return $this->belongsTo(Product::class,'title','id');
    }

}
