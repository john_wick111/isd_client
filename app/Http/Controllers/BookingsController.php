<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use App\Sport;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use App\Booking;
use App\OrderProduct;
use App\Product;
use Carbon;
use Illuminate\Support\Facades\Auth;
use App;
use PDF;
use App\Payment;
use DB;

class BookingsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }
            if(isset($_GET['users']) && $_GET['users'] > 0 ){
                $query = $query->where('client_id',$_GET['users']);
            }
            if(isset($_GET['court']) && $_GET['court'] > 0 ){
                $query = $query->where('pitch',$_GET['court']);
            }
            if(isset($_GET['sport']) && $_GET['sport'] > 0 ){
                $query = $query->where('sports',  'LIKE', "%{$_GET['sport']}%");
            }
            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }


        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }

    public function CheckBook(Request $request)
    {
        $check = Booking::where('date',$request->date)
                        ->whereTime('from','>=',\Carbon\Carbon::parse($request->from))
                        ->whereTime('to','<=',\Carbon\Carbon::parse($request->to))
                        ->where('pitch',$request->pitch)
                        ->first();
        if($check === null){
            return response()->json([
                'state'=>true,
            ]);
        }else{
            return response()->json([
                'state'=>false,
                'msg' => 'This Pitch Aleardy Booked in this time',
            ]);
        }
    }

    public function CheckProduct($id)
    {
        $data = DB::table('products')->where('id',$id)->first();
        return response()->json([
            'data'=>$data,
        ]);
    }

    public function active(Request $request){
        $update=  DB::table('users')->where('id', $_GET['id'])->where('email', $_GET['email'])->where('activation_code', $_GET['code'])->update(['status' => 1]);
        return view('emails.welcome');
    }

    public function store(Request $request)
    {
            $date = $request->date;
            $from = $request->from;
            $to = $request->to;
            $pitch = $request->pitch;
            $size = $request->size;
            $price = $request->price;
            $discount = $request->discount;
            $total = $request->total;
            $sports = $request->sports;
            if($request->id){
            }else{
                $allSize = Booking::where('date',$date)->whereTime('from','<=',\Carbon\Carbon::parse($from))->whereTime('to','>=',\Carbon\Carbon::parse($to))->where('pitch',$pitch)->sum('size');
                $allSize += $size;
                if($allSize > 100){
                    return response()->json([
                        'state'=>400,
                        'msg' => "you can't book over 100%",
                    ]);              
                }
            }
            $check = Booking::where('date',$date)->whereTime('from','>=',\Carbon\Carbon::parse($from))->whereTime('to','<=',\Carbon\Carbon::parse($to))->where('pitch',$pitch)->first();
            if($check === null){
               if($request->id){
                $court_times = Booking::find($request->id);
               }else{
                $court_times = new Booking();
               }
                $court_times->date = $date;
                $court_times->from = $from;
                $court_times->to = $to;
                $court_times->pitch = $pitch;
                $court_times->size = $size;
                $court_times->price = $price;
                $court_times->dicsount = $discount;
                $court_times->total = $total;
                $court_times->client_id = $request->client_id;
                $court_times->status = $request->status;
                $court_times->penalty = $request->penalty;
                $court_times->sports = $sports;
                $court_times->code = rand ( 10000 , 99999 );
                $court_times->save();
                $products_details = [];
                if($request->title[0] > 0){
                    for($i= 0; $i < count($request->title); $i++){
                        $OrderProduct = new OrderProduct();
                        $OrderProduct->title = $request->title[$i];
                        $OrderProduct->price = $request->price_product[$i];
                        $OrderProduct->total = $request->total_product[$i];
                        $OrderProduct->count = $request->count[$i];
                        $OrderProduct->book_id  = $court_times->id;
                        $OrderProduct->save();
                        $product = Product::where('id',$request->title[$i])->first();
                        $product->decrement('stock', $request->count[$i]);
                    }     
                }   
            }else{
            return response()->json([
                'state'=>400,
                'msg' => "This Pitch Aleardy Booked in this time",
            ]);    
            }
        return response()->json([
            'state'=>200,
            'msg' => "Data Insert Success",
        ]);  
    }
    
    public function AddPay(Request $request)
    {
        $payment = new Payment();
        $payment->amount = $request->amount;
        $payment->type = $request->type;
        $payment->book_id = $request->book_id;
        $payment->client_id = $request->client_id;
        $payment->ref = $request->ref;
        $payment->status = 1;
        $payment->save();
        return redirect('admin/bookings');

    }

    public function ExportInvoice($id)
    {
        $booking = Booking::find($id);
        $details = [
            'created_at'  => $booking->created_at,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'client_id' => $booking->client_id,
            'code'  => $booking->code,
        ];
        
        
        $pdf = PDF::loadView('emails.tax',compact('details'));

        return $pdf->download('invoice'.$id.'.pdf');
    }

    public function ExportReceipt($id)
    {
        $booking = Booking::find($id);
        $name = DB::table('users')->where('id',$booking->client_id)->value('name');
        $email = DB::table('users')->where('id',$booking->client_id)->value('email');          
        $details = [
            'code'  => $booking->code,
            'date'  => $booking->date,
            'from'  => $booking->from,
            'to'    => $booking->to,
            'pitch' => $booking->pitch,
            'sports'=> $booking->sports,
            'total' => $booking->total,
            'name' => $name,
            'email' => $email,
            'date' => $booking->created_at, 
            'client_id' => $booking->client_id,
            'id' => $id,            
        ];
        $pdf = PDF::loadView('emails.receipt',compact('details'));
        return $pdf->download('receipt'.$id.'.pdf');
    }


    public function DeleteOrderProduct($id)
    {
        App\OrderProduct::where('id',$id)->delete();
        return redirect()->back();
    }

     public function bookings(){
         
       $sports=Sport::all();
       return view('pages.booking',['sports'=>$sports]);

    }
   
}