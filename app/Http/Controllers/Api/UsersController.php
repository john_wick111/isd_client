<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use Response;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Auth;
use Hash;
use App;
use App\Http\Controllers\Controller;
use App\Favourite;
use App\Review;

class UsersController extends Controller
{

    public function Users()
    {
        $user = User::all();
        $response['status'] = 200;
        $response['data'] = $user;
        return $response;
    }


    public function User()
    {
        $user = User::find(Auth::id());
        $response['status'] = 200;
        $response['data'] = $user;
        return $response;
    }

    
    public function UsersAddFavourite(Request $request){
        $check = Favourite::where('client_id',Auth::user()->id)->where('court_id',$request->court_id)->count();
        if($check > 0){
            Favourite::where('client_id',Auth::user()->id)->where('court_id',$request->court_id)->delete();
            $response['status'] = 200;
            $response['data'] = "";
            $response['msg'] = "Remove From Favorites";
            return $response;
        }else{
            $data = new Favourite();
            $data->client_id = Auth::user()->id;
            $data->court_id = $request->court_id;
            $data->save();
            $response['status'] = 200;
            $response['data'] = $data;
            $response['msg'] = "Added To Favorites";
            return $response;
        }
    } 

    public function UsersAddReview(Request $request){
        $check = Review::where('client_id',Auth::user()->id)->where('court_id',$request->court_id)->where('book_id',$request->book_id)->count();
        if($check > 0){  
            $response['status'] = 202;
            $response['data'] = "";
            $response['msg'] = "you Add Review Before";
            return $response;
        }else{      
            $data = new Review();
            $data->client_id = $request->client_id;
            $data->court_id = $request->court_id;
            $data->book_id = $request->book_id;
            $data->comment = $request->comment;
            $data->reviews = $request->reviews;
            $data->save();
        }
        $response['status'] = 200;
        $response['data'] = $data;
        return $response;
    }

    public function UsersLiked(){
        $data = Favourite::where('client_id',Auth::user()->id)->with('court')->get();
        $response['status'] = 200;
        $response['data'] = $data;
        return $response;
    }

    public function UsersReview(){
        $data = Review::where('client_id',Auth::user()->id)->get();
        $response['status'] = 200;
        $response['data'] = $data;
        return $response;
    }

    public function EditeUser(Request $request)
    {
        $User = User::findOrFail(Auth::user()->id);
        $User->email = $request->email;
        $User->name = $request->name;
        $User->contacts = $request->phone;
        if ($request->avatar){
            $safeName = 'new'.date("Y-m-d-H-i-s").'.png';
            $img = $this->base64ToImage($request->avatar,public_path().'/storage/users/'.$safeName);
            $User->avatar = 'users/'.$safeName;
        }
        if($request->password){
            $User->password = Hash::make($request->password);
        }
        $User->save();
        $response['status'] = 200;
        $response['msg']        = 'Profile Updated ';
        $response['data'] = $User;
        return $response;
    }

    function base64ToImage($base64_string, $output_file) {
        $file = fopen($output_file, "wb");
    
        $data = explode(',', $base64_string);
    
        fwrite($file, base64_decode($data[1]));
        fclose($file);
    
        return $output_file;
    }

    public function Show($id)
    {
        $user = User::find($id);
        $response['status'] = 200;
        $response['data'] = $user;
        return $response;
    }
    
    public function DeleteUser($id)
    {
        $user = User::findOrFail($id)->delete();
        $response['status'] = 200;
        $response['data'] = $user;
        return $response;
    }

    public function UpdateToken(Request $request){
        User::where('id',$request->id_user)->update(['token' => $request->token]);

    }

    public function SendNotifcation(Request $request){
        $title = $request->title;
        $token = User::where('id',$request->id)->value('token');
        $content = array("en" => $title);
        $fields = array(
            'app_id' => "aa840492-7879-433c-9f9b-55802a90045b",
            'data' => array("foo" => "bar"),
            'large_icon' =>"ic_launcher_round.png",
            'include_player_ids' => array($token),
            'contents' => $content
        );
    
        $fields = json_encode($fields);        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                'Authorization: Basic N2NjNGIwM2QtZjAyNC00ODM5LWE0YzUtMzBhOWZkOTI1ZmM4'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
    
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function SendNotifcationAll(Request $request){
        $title = $request->title;
        $content = array("en" => $title);
        $fields = array(
            'app_id' => "aa840492-7879-433c-9f9b-55802a90045b",
            'included_segments' => ["All"],
            'data' => array("foo" => "bar"),
            'large_icon' =>"ic_launcher_round.png",
            'contents' => $content
        );
    
        $fields = json_encode($fields);        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                'Authorization: Basic N2NjNGIwM2QtZjAyNC00ODM5LWE0YzUtMzBhOWZkOTI1ZmM4'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
    
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
