<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    
    public function details(): HasOne
    {
        return $this->HasOne(OrderProduct::class,'id');
    }

    public function sport()
    {
        return $this->belongsToMany('App\Sport', 'povite_sports_products');    
    }
}
