<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Court extends Model
{
    protected $appends = ['sizeName'];

    public function getSizeNameAttribute($v)
    {
        $name = CourtsSize::where('size',$this->size)->value('name');
         return $name;
    }

    public function sport()
    {
        return $this->belongsToMany('App\Sport', 'povite_courts_sports', 'court_id', 'sport_id');
    }

    public function scopeParent($query)
    {
        return $query->whereNull('parent');
    }

    public function Times()
    {
        return $this->hasMany('App\Court_times', 'court_id', 'id');
    }

    public function getImageAttribute($v)
    {
        if($v){
            return 'http://bookings.isddubai.com/public/storage/'.$v;
         }
    }

    public function getIsFavAttribute(){
        return Favourite::where('client_id', Auth::id())->where('court_id',$this->id)->count();;
    }
}
